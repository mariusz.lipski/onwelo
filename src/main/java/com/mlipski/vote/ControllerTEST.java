package com.mlipski.vote;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerTEST {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
}